# dast-project

The DAST project requires a linked (but not necessarily GitLab-managed) K8s cluster. The pipeline is setup to deploy review apps for each build. In this case, 
just a base Apache server Helm install.

* There is an [MR](https://gitlab.com/glen_miller/dast-project/-/merge_requests/6) for remediation of a Container Scanning issue.
* There is an [MR](https://gitlab.com/glen_miller/dast-project/-/merge_requests/5) just designed for showing basic MR functionality.

API and Container Fuzzing are enabled, but don't really do much at this time.
